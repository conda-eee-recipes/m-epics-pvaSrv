m-epics-pvaSrv conda recipe
===========================

Home: https://bitbucket.org/europeanspallationsource/m-epics-pvaSrv

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: EPICS pvaSrv module
